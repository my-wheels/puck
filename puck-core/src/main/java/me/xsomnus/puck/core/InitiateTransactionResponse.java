package me.xsomnus.puck.core;

import lombok.Data;
import lombok.experimental.Accessors;

/**
 * @author @叶小夏
 * @since 2019/8/5 0005 11:34
 * - 才需学也,学需静也/非淡泊无以明志，非宁静无以致远
 */
@Data
@Accessors(chain = true)
public class InitiateTransactionResponse {
    private String xid;
}
