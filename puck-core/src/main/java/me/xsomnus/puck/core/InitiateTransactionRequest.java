package me.xsomnus.puck.core;

import lombok.Data;

import java.util.List;

/**
 * @author @叶小夏
 * @since 2019/8/5 0005 11:31
 * - 才需学也,学需静也/非淡泊无以明志，非宁静无以致远
 */
@Data
public class InitiateTransactionRequest {

    private String name;

    private String groupId;

    List<Participant0> participants;

}
