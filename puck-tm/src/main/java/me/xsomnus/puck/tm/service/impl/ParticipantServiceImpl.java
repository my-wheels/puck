package me.xsomnus.puck.tm.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import me.xsomnus.puck.tm.entity.Participant;
import me.xsomnus.puck.tm.mapper.ParticipantMapper;
import me.xsomnus.puck.tm.service.IParticipantService;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author @叶小夏
 * @since 2019/8/2 0002 11:27
 * - 才需学也,学需静也/非淡泊无以明志，非宁静无以致远
 */
@Service
public class ParticipantServiceImpl extends ServiceImpl<ParticipantMapper, Participant> implements IParticipantService {

    @Override
    public List<Participant> listByXid(String xid) {
        return this.list(new QueryWrapper<Participant>().eq(Participant.XID, xid));
    }

}
