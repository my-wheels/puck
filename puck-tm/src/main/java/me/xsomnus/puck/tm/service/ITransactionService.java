package me.xsomnus.puck.tm.service;

import com.baomidou.mybatisplus.extension.service.IService;
import me.xsomnus.puck.tm.entity.Transaction;

/**
 * @author @叶小夏
 * @since 2019/8/2 0002 11:26
 * - 才需学也,学需静也/非淡泊无以明志，非宁静无以致远
 */
public interface ITransactionService extends IService<Transaction> {
}
