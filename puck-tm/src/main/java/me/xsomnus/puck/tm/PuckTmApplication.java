package me.xsomnus.puck.tm;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author @叶小夏
 * @since 2019/8/2 0002 11:30
 * - 才需学也,学需静也/非淡泊无以明志，非宁静无以致远
 */
@SpringBootApplication
public class PuckTmApplication {
    public static void main(String[] args) {
        SpringApplication.run(PuckTmApplication.class, args);
    }
}
