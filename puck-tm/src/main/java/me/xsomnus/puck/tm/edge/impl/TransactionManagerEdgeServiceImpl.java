package me.xsomnus.puck.tm.edge.impl;

import com.baomidou.mybatisplus.core.toolkit.IdWorker;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import me.xsomnus.puck.core.InitiateTransactionRequest;
import me.xsomnus.puck.core.InitiateTransactionResponse;
import me.xsomnus.puck.tm.core.ExecutionCallback;
import me.xsomnus.puck.tm.core.TransactionCaller;
import me.xsomnus.puck.tm.edge.TransactionManagerEdgeService;
import me.xsomnus.puck.tm.entity.Participant;
import me.xsomnus.puck.tm.entity.Transaction;
import me.xsomnus.puck.tm.entity.TransactionLog;
import me.xsomnus.puck.tm.pojo.enums.ExecutionPhase;
import me.xsomnus.puck.tm.pojo.enums.ParticipantRole;
import me.xsomnus.puck.tm.pojo.enums.RPCType;
import me.xsomnus.puck.tm.pojo.enums.TccStatus;
import me.xsomnus.puck.tm.service.IParticipantService;
import me.xsomnus.puck.tm.service.ITransactionLogService;
import me.xsomnus.puck.tm.service.ITransactionService;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @author @叶小夏
 * @since 2019/8/2 0002 11:36
 * - 才需学也,学需静也/非淡泊无以明志，非宁静无以致远
 */
@Slf4j
@Service
@AllArgsConstructor
public class TransactionManagerEdgeServiceImpl implements TransactionManagerEdgeService {

    private final ITransactionService iTransactionService;
    private final IParticipantService iParticipantService;
    private final ITransactionLogService iTransactionLogService;
    private TransactionCaller transactionCaller;

    @Override
    public InitiateTransactionResponse initiate(InitiateTransactionRequest request) {
        String xid = IdWorker.getIdStr();
        final Transaction transaction = new Transaction()
                .setXid(xid)
                .setGroupId(request.getGroupId())
                .setStatus(TccStatus.TRY)
                .setMaxRetryTimes(5)
                .setRetryInterval(30)
                .setRetriedTimes(0)
                .setName(request.getName());
        final List<Participant> participantList = request.getParticipants().stream().map(participant0 ->
                new Participant().setName(participant0.getName())
                        .setRole(participant0.isLeader() ? ParticipantRole.LEADER : ParticipantRole.FOLLOWER)
                        .setRpcType(RPCType.HTTP)
                        .setXid(xid)
                        .setUrl(participant0.getUri())
                        .setRequestBody("none")
        ).collect(Collectors.toList());
        boolean ret = iTransactionService.save(transaction) && iParticipantService.saveBatch(participantList);
        if (ret) {
            return new InitiateTransactionResponse().setXid(xid);
        }
        return new InitiateTransactionResponse().setXid("error");
    }

    @Override
    public void confirm(String xid) {
        final Transaction transaction = iTransactionService.getById(xid);
        final List<Participant> participantList = iParticipantService.listByXid(xid);
        final LocalDateTime startTime = LocalDateTime.now();
        List<TransactionLog> logs = new ArrayList<>(participantList.size());

        // Mark --- 确认执行所有的事务
        transactionCaller.exec(transaction, participantList, (result, participantId, reason) -> {
            log.debug("执行结果回调, result = {}, 参与者 = {}, 原因 = {}", result, participantId, reason);
            logs.add(new TransactionLog()
                    .setId(IdWorker.getId())
                    .setXid(transaction.getXid())
                    .setExecutionPhase(ExecutionPhase.TWO)
                    .setParticipantId(participantId)
                    .setExecutionSeq(transaction.getRetriedTimes())
                    .setFailCause(reason)
                    .setExecutionResult(result)
                    .setStartTime(startTime)
                    .setEndTime(LocalDateTime.now()));
        });
        // Mark --- 保存事务执行日志
        iTransactionLogService.saveBatch(logs);
    }

    @Override
    public void cancel(String xid) {
        final Transaction transaction = iTransactionService.getById(xid);
        final List<Participant> participantList = iParticipantService.listByXid(xid);
        transactionCaller.exec(transaction, participantList, new ExecutionCallback() {
            @Override
            public void execCallback(String result, String participantId, String reason) {
                // ignore
            }
        });
    }
}
