package me.xsomnus.puck.tm.core;

/**
 * @author @叶小夏
 * @since 2019/8/2 0002 14:11
 * - 才需学也,学需静也/非淡泊无以明志，非宁静无以致远
 *
 * <pre>
 *     执行结果的回调
 * </pre>
 */
public interface ExecutionCallback {

    void execCallback(String result, String participantId, String reason);
}
