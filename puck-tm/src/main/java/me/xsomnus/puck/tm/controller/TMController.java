package me.xsomnus.puck.tm.controller;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import me.xsomnus.puck.core.InitiateTransactionRequest;
import me.xsomnus.puck.core.InitiateTransactionResponse;
import me.xsomnus.puck.tm.edge.TransactionManagerEdgeService;
import org.springframework.web.bind.annotation.*;

/**
 * @author @叶小夏
 * @since 2019/8/2 0002 11:35
 * - 才需学也,学需静也/非淡泊无以明志，非宁静无以致远
 */
@Slf4j
@AllArgsConstructor
@RestController
@RequestMapping("/api/v1/tm")
public class TMController {

    private final TransactionManagerEdgeService tmService;


    /**
     * 发起一个事务
     */
    @PostMapping("/initiate")
    public InitiateTransactionResponse initiate(@RequestBody InitiateTransactionRequest request) {
        log.debug("发起事务请求体 request: {}" , request);
        return tmService.initiate(request);
    }

    /**
     * 确认（提交）事务
     *
     * @param xid 事务编号
     */
    @PutMapping("/confirm/{xid}")
    public void confirm(@PathVariable String xid) {
        tmService.confirm(xid);
    }

    /**
     * 取消（回滚）事务
     * @param xid 事务编号
     */
    @PutMapping("/cancel/{xid}")
    public void cancel(@PathVariable String xid) {
        tmService.cancel(xid);
    }

}
