package me.xsomnus.puck.tm.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import me.xsomnus.puck.tm.pojo.enums.TccStatus;

import java.time.LocalDateTime;

/**
 * @author @叶小夏
 * @since 2019/8/2 0002 11:02
 * - 才需学也,学需静也/非淡泊无以明志，非宁静无以致远
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
public class Transaction extends Model<Transaction> {

    @TableId
    private String xid;

    private String name;

    private String groupId;

    private Integer maxRetryTimes;

    private Integer retriedTimes;

    private Integer retryInterval;

    private LocalDateTime nextRetryTime;

    private TccStatus status;

    private Boolean enabled;

    private Boolean deleted;

    private Integer version;

    private LocalDateTime createTime;

    private LocalDateTime updateTime;

}
