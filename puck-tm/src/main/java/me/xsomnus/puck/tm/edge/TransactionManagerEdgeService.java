package me.xsomnus.puck.tm.edge;

import me.xsomnus.puck.core.InitiateTransactionRequest;
import me.xsomnus.puck.core.InitiateTransactionResponse;

/**
 * @author @叶小夏
 * @since 2019/8/2 0002 11:35
 * - 才需学也,学需静也/非淡泊无以明志，非宁静无以致远
 */
public interface TransactionManagerEdgeService {

    /**
     * 发起事务
     * @param request 发起事务请求方
     * @return 此次事务编号
     */
    InitiateTransactionResponse initiate(InitiateTransactionRequest request);

    /**
     * 事务确认
     */
    void confirm(String xid);

    /**
     * 事务取消
     */
    void cancel(String xid);
}
