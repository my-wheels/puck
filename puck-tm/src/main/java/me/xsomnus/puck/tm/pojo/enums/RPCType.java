package me.xsomnus.puck.tm.pojo.enums;

import com.baomidou.mybatisplus.core.enums.IEnum;

/**
 * @author @叶小夏
 * @since 2019/8/2 0002 11:00
 * - 才需学也,学需静也/非淡泊无以明志，非宁静无以致远
 */
public enum RPCType implements IEnum<Short> {

    HTTP((short) 100),
    DUBBO((short) 102),
    GRPC((short) 103),
    THRIF((short) 104),
    CUSTOM((short) 200);

    private final short code;

    RPCType(short code) {
        this.code = code;
    }

    @Override
    public Short getValue() {
        return code;
    }
}
