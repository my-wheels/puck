package me.xsomnus.puck.tm.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import me.xsomnus.puck.tm.entity.TransactionLog;
import me.xsomnus.puck.tm.mapper.TransactionLogMapper;
import me.xsomnus.puck.tm.service.ITransactionLogService;
import org.springframework.stereotype.Service;

/**
 * @author @叶小夏
 * @since 2019/8/2 0002 11:26
 * - 才需学也,学需静也/非淡泊无以明志，非宁静无以致远
 */
@Service
public class TransactionLogServiceImpl extends ServiceImpl<TransactionLogMapper, TransactionLog> implements ITransactionLogService {
}
