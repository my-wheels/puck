package me.xsomnus.puck.tm.core;

import com.alibaba.fastjson.JSONObject;
import me.xsomnus.puck.tm.entity.Participant;
import me.xsomnus.puck.tm.entity.Transaction;
import okhttp3.*;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.util.List;
import java.util.concurrent.TimeUnit;

/**
 * @author @叶小夏
 * @since 2019/8/2 0002 13:56
 * - 才需学也,学需静也/非淡泊无以明志，非宁静无以致远
 */
@Component
public class DefaultTransactionCaller implements TransactionCaller {


    private static OkHttpClient okHttpClient;

    static {
        okHttpClient = new OkHttpClient.Builder()
                .connectTimeout(5, TimeUnit.SECONDS)
                .build();
    }

    @Override
    public void exec(Transaction transaction, List<Participant> participants, ExecutionCallback executionCallback) {
        participants.forEach(participant -> {
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("xid", transaction.getXid());
            final Call call = okHttpClient.newCall(new Request.Builder()
                    .url(participant.getUrl() + "/confirm")
                    .put(RequestBody.create(MediaType.parse("application/json"), jsonObject.toJSONString()))
                    .build());
            try {
                final Response response = call.execute();
                final int code = response.code();
                if (code == 200) {
                    executionCallback.execCallback("success", participant.getId(), "成功");
                } else {
                    executionCallback.execCallback("failed", participant.getId(), "失败");
                }
            } catch (IOException e) {
                executionCallback.execCallback("failed", participant.getId(), e.getLocalizedMessage());
            }
        });
    }
}
