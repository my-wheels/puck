package me.xsomnus.puck.tm.utils;

import java.util.HashMap;
import java.util.Map;

/**
 * @author @叶小夏
 * @since 2019/8/2 0002 14:34
 * - 才需学也,学需静也/非淡泊无以明志，非宁静无以致远
 */
public class HttpUtils {

    public static Map<String, String> buildMap(String str) {
        final String[] split = str.split("&");
        Map<String, String> retMap = new HashMap<>(split.length);
        for (int i = split.length - 1; i >= 0; i--) {
            String tmp = split[i];
            final String[] subSplit = tmp.split("=");
            retMap.put(subSplit[0], subSplit[1]);
        }
        return retMap;
    }

    public static String buildStr(Map<String, String> map) {
        return null;
    }
}
