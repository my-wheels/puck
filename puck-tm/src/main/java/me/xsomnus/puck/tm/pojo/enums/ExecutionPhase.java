package me.xsomnus.puck.tm.pojo.enums;

import com.baomidou.mybatisplus.core.enums.IEnum;

/**
 * @author @叶小夏
 * @since 2019/8/2 0002 11:22
 * - 才需学也,学需静也/非淡泊无以明志，非宁静无以致远
 */
public enum ExecutionPhase implements IEnum<Short> {

    ONE((short)1),
    TWO((short)2);

    private final short code;

    ExecutionPhase(short code) {
        this.code = code;
    }

    @Override
    public Short getValue() {
        return code;
    }
}
