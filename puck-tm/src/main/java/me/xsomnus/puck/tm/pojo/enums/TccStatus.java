package me.xsomnus.puck.tm.pojo.enums;

import com.baomidou.mybatisplus.core.enums.IEnum;

/**
 * @author @叶小夏
 * @since 2019/8/2 0002 11:05
 * - 才需学也,学需静也/非淡泊无以明志，非宁静无以致远
 */
public enum TccStatus implements IEnum<Short> {
    TRY((short) 1),
    confrimed((short) 200),
    confirmRetring((short) 201),
    CANCELled((short) 300),
    cancelRetried((short) 301);

    private final short code;

    TccStatus(short code) {
        this.code = code;
    }

    @Override
    public Short getValue() {
        return code;
    }
}
