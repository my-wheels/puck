package me.xsomnus.puck.tm.pojo.enums;

import com.baomidou.mybatisplus.core.enums.IEnum;

/**
 * @author @叶小夏
 * @since 2019/8/2 0002 10:58
 * - 才需学也,学需静也/非淡泊无以明志，非宁静无以致远
 */
public enum ParticipantRole implements IEnum<Short> {

    LEADER((short) 0),
    FOLLOWER((short) 1);

    private final short code;

    ParticipantRole(short code) {
        this.code = code;
    }

    @Override
    public Short getValue() {
        return code;
    }
}
