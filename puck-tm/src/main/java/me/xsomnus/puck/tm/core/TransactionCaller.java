package me.xsomnus.puck.tm.core;

import me.xsomnus.puck.tm.entity.Participant;
import me.xsomnus.puck.tm.entity.Transaction;

import java.util.List;

/**
 * @author @叶小夏
 * @since 2019/8/2 0002 13:51
 * - 才需学也,学需静也/非淡泊无以明志，非宁静无以致远
 */
public interface TransactionCaller {

    void exec(Transaction transaction, List<Participant> participants, ExecutionCallback executionCallback);

}
