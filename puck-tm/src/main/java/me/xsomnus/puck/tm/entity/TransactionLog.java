package me.xsomnus.puck.tm.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import me.xsomnus.puck.tm.pojo.enums.ExecutionPhase;

import java.time.LocalDateTime;

/**
 * @author @叶小夏
 * @since 2019/8/2 0002 11:16
 * - 才需学也,学需静也/非淡泊无以明志，非宁静无以致远
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
public class TransactionLog extends Model<TransactionLog> {

    @TableId
    private Long id;

    private String xid;

    private String participantId;

    private ExecutionPhase executionPhase;

    private String executionResult;

    private Integer executionSeq;

    private String failCause;

    private Boolean success;

    private LocalDateTime createTime;

    private LocalDateTime updateTime;

    private LocalDateTime startTime;

    private LocalDateTime endTime;

    private String finalResult;
}
