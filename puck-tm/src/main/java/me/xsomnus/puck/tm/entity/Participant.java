package me.xsomnus.puck.tm.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.core.toolkit.IdWorker;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import me.xsomnus.puck.tm.pojo.enums.ParticipantRole;
import me.xsomnus.puck.tm.pojo.enums.RPCType;

/**
 * @author @叶小夏
 * @since 2019/8/2 0002 10:55
 * - 才需学也,学需静也/非淡泊无以明志，非宁静无以致远
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
public class Participant extends Model<Participant> {

    @TableId(type = IdType.ID_WORKER_STR)
    private String id;

    private String xid;

    private ParticipantRole role;

    private RPCType rpcType;

    private String name;

    private String url;

    private String requestBody;

    public static final String XID = "xid";

}
