package me.xsomnus.puck.samples.product.entity;

import com.baomidou.mybatisplus.extension.activerecord.Model;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * @author @叶小夏
 * @since 2019/8/1 0001 16:32
 * - 才需学也,学需静也/非淡泊无以明志，非宁静无以致远
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
public class Product extends Model<Product> {

    private Long id;

    private String name;

    private Integer stock;

    private Integer unitPrice;
}
