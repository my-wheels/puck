package me.xsomnus.puck.samples.product.service;

import com.baomidou.mybatisplus.extension.service.IService;
import me.xsomnus.puck.samples.common.pojo.request.ProductResourceReservationRequest;
import me.xsomnus.puck.samples.common.pojo.response.ProductResourceReservationResponse;
import me.xsomnus.puck.samples.product.entity.Product;

/**
 * @author @叶小夏
 * @since 2019/8/1 0001 16:37
 * - 才需学也,学需静也/非淡泊无以明志，非宁静无以致远
 */
public interface IProductService extends IService<Product> {

    ProductResourceReservationResponse reserve(ProductResourceReservationRequest request);

    void cancel(ProductResourceReservationRequest request);

    void confirm(ProductResourceReservationRequest request);
}
