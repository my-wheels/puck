package me.xsomnus.puck.samples.product.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import me.xsomnus.puck.samples.product.entity.ProductStockTcc;
import me.xsomnus.puck.samples.product.mapper.ProductStockTccMapper;
import me.xsomnus.puck.samples.product.service.IProductStockTccService;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author @叶小夏
 * @since 2019/8/2 0002 16:57
 * - 才需学也,学需静也/非淡泊无以明志，非宁静无以致远
 */
@Service
public class ProductStockTccServiceImpl extends ServiceImpl<ProductStockTccMapper, ProductStockTcc> implements IProductStockTccService {

    @Override
    public List<ProductStockTcc> listByXid(String xid) {
        return this.list(new QueryWrapper<ProductStockTcc>().eq("xid", xid));
    }
}
