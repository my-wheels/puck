package me.xsomnus.puck.samples.product.controller;

import lombok.AllArgsConstructor;
import me.xsomnus.puck.samples.common.pojo.request.ProductResourceReservationRequest;
import me.xsomnus.puck.samples.common.pojo.response.ProductResourceReservationResponse;
import me.xsomnus.puck.samples.product.service.IProductService;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author @叶小夏
 * @since 2019/8/2 0002 16:03
 * - 才需学也,学需静也/非淡泊无以明志，非宁静无以致远
 */
@RestController
@RequestMapping("/product")
@AllArgsConstructor
public class ProductController {

    private final IProductService iProductService;

    @PutMapping("/reservation")
    public ProductResourceReservationResponse reservationProductResource(@RequestBody ProductResourceReservationRequest request) {
        return iProductService.reserve(request);
    }

    @PutMapping("/confirm")
    public void confirm(@RequestBody ProductResourceReservationRequest request) {
        iProductService.confirm(request);
    }

    @PutMapping("/cancel")
    public void cancel(@RequestBody ProductResourceReservationRequest request) {
        iProductService.cancel(request);
    }
}
