package me.xsomnus.puck.samples.product.service.impl;

import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import lombok.AllArgsConstructor;
import me.xsomnus.puck.samples.common.pojo.ProductDTO;
import me.xsomnus.puck.samples.common.pojo.request.ProductResourceReservationRequest;
import me.xsomnus.puck.samples.common.pojo.response.ProductResourceReservationResponse;
import me.xsomnus.puck.samples.product.entity.Product;
import me.xsomnus.puck.samples.product.entity.ProductStockTcc;
import me.xsomnus.puck.samples.product.mapper.ProductMapper;
import me.xsomnus.puck.samples.product.pojo.enums.TccStatus;
import me.xsomnus.puck.samples.product.service.IProductService;
import me.xsomnus.puck.samples.product.service.IProductStockTccService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.stream.Collectors;

/**
 * @author @叶小夏
 * @since 2019/8/1 0001 16:37
 * - 才需学也,学需静也/非淡泊无以明志，非宁静无以致远
 */
@Service
@AllArgsConstructor
public class ProductServiceImpl extends ServiceImpl<ProductMapper, Product> implements IProductService {

    private final IProductStockTccService iProductStockTccService;

    @Override
    @Transactional(rollbackFor = Exception.class)
    public ProductResourceReservationResponse reserve(ProductResourceReservationRequest request) {

        final Set<Long> productIds = request.getItems().stream().map(ProductResourceReservationRequest.Product3RItem::getProductId).collect(Collectors.toSet());
        final Map<Long, Integer> stockMap = request.getItems().stream().collect(Collectors.toMap(ProductResourceReservationRequest.Product3RItem::getProductId, ProductResourceReservationRequest.Product3RItem::getQuantity, (k1, k2) -> k1));
        final Collection<Product> products = this.listByIds(productIds);
        final boolean sufficient = products.stream()
                .allMatch(product -> product.getStock() >= stockMap.get(product.getId()));
        final Map<Long, Integer> unitPriceMap = products.stream().collect(Collectors.toMap(Product::getId, Product::getUnitPrice, (k1, k2) -> k1));

        ProductResourceReservationResponse product3r = new ProductResourceReservationResponse();
        if (sufficient) {
            // Mark --- 扣库存
            final List<Product> updateProductList = products.stream()
                    .map(product -> new Product()
                            .setStock(product.getStock() - stockMap.get(product.getId()))
                            .setId(product.getId()))
                    .collect(Collectors.toList());

            List<ProductStockTcc> productStockTccs = request.getItems().stream()
                    .map(product3RItem -> new ProductStockTcc()
                            .setProductId(product3RItem.getProductId())
                            .setStock(product3RItem.getQuantity())
                            .setStatus(TccStatus.TRY)
                            .setXid(request.getXid()))
                    .collect(Collectors.toList());
            // Mark --- 保存事务 try阶段
            iProductStockTccService.saveBatch(productStockTccs);
            this.updateBatchById(updateProductList);
            product3r.setOk(true);
            Map<Long, ProductDTO> productMap = new HashMap<>();
            request.getItems().forEach(item -> productMap.put(item.getProductId(), new ProductDTO()
                    .setProductId(item.getProductId())
                    .setUnitPrice(unitPriceMap.get(item.getProductId()))));
            product3r.setProductMap(productMap);
        } else {
            product3r.setOk(false);
            product3r.setFailReason("库存不足");
        }
        return product3r;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void cancel(ProductResourceReservationRequest request) {
        final List<ProductStockTcc> productStockTccs = iProductStockTccService.listByXid(request.getXid());
        final boolean allMatch = productStockTccs.stream().allMatch(stockTcc -> stockTcc.getStatus().equals(TccStatus.TRY));
        if (allMatch) {
            List<ProductStockTcc> stockTccUpdate = new ArrayList<>(productStockTccs.size());
            AtomicBoolean flag = new AtomicBoolean(true);
            productStockTccs.forEach(productStockTcc -> {
                final ProductStockTcc tcc = new ProductStockTcc().setStatus(TccStatus.CANCEL)
                        .setId(productStockTcc.getId());
                stockTccUpdate.add(tcc);
                if (flag.get()) {
                    flag.set(this.update(new UpdateWrapper<Product>().setSql("stock  = stock + " + productStockTcc.getStock()).eq("id", productStockTcc.getProductId())));
                }
            });
            iProductStockTccService.updateBatchById(stockTccUpdate);
            return;
        }
        // todo 添加一个tcc异常类
        throw new IllegalArgumentException("cancel reservation failed because of tcc status is not in phase try");
    }

    @Override
    public void confirm(ProductResourceReservationRequest request) {
        final List<ProductStockTcc> productStockTccs = iProductStockTccService.listByXid(request.getXid());
        final boolean allMatch = productStockTccs.stream().allMatch(stockTcc -> stockTcc.getStatus().equals(TccStatus.TRY));
        if (allMatch) {
            List<ProductStockTcc> stockTccUpdate = new ArrayList<>(productStockTccs.size());
            productStockTccs.forEach(productStockTcc -> stockTccUpdate.add(new ProductStockTcc().setStatus(TccStatus.CONFIRM).setId(productStockTcc.getId())));
            iProductStockTccService.updateBatchById(stockTccUpdate);
            return;
        }
        // todo 添加一个tcc异常类
        throw new IllegalArgumentException("confirm reservation failed because of tcc status is not in phase try");
    }

}
