package me.xsomnus.puck.samples.product.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import me.xsomnus.puck.samples.product.pojo.enums.TccStatus;

/**
 * @author @叶小夏
 * @since 2019/8/2 0002 16:53
 * - 才需学也,学需静也/非淡泊无以明志，非宁静无以致远
 */
@Data
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = true)
public class ProductStockTcc extends Model<ProductStockTcc> {

    @TableId(type = IdType.ID_WORKER)
    private Long id;
    private String xid;
    private Long productId;
    private Integer stock;
    private TccStatus status;
}
