package me.xsomnus.puck.samples.product;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author @叶小夏
 * @since 2019/8/1 0001 15:40
 * - 才需学也,学需静也/非淡泊无以明志，非宁静无以致远
 */
@SpringBootApplication
public class PuckProductApplication {
    public static void main(String[] args) {
        SpringApplication.run(PuckProductApplication.class, args);
    }
}
