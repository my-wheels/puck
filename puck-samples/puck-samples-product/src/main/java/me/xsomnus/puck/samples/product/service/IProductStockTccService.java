package me.xsomnus.puck.samples.product.service;

import com.baomidou.mybatisplus.extension.service.IService;
import me.xsomnus.puck.samples.product.entity.ProductStockTcc;

import java.util.List;

/**
 * @author @叶小夏
 * @since 2019/8/2 0002 16:57
 * - 才需学也,学需静也/非淡泊无以明志，非宁静无以致远
 */
public interface IProductStockTccService extends IService<ProductStockTcc> {

    // Mark --- 本地资源管理器

    /**
     * 根据事务编号，获取事务的对应的详情
     * @param xid 事务编号
     */
    List<ProductStockTcc> listByXid(String xid);
}
