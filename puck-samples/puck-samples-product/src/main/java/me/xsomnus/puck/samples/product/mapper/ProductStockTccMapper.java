package me.xsomnus.puck.samples.product.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import me.xsomnus.puck.samples.product.entity.ProductStockTcc;

/**
 * @author @叶小夏
 * @since 2019/8/2 0002 16:57
 * - 才需学也,学需静也/非淡泊无以明志，非宁静无以致远
 */
public interface ProductStockTccMapper extends BaseMapper<ProductStockTcc> {
}
