package me.xsomnus.puck.samples.account.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.time.LocalDateTime;

/**
 * @author @叶小夏
 * @since 2019/8/1 0001 15:42
 * - 才需学也,学需静也/非淡泊无以明志，非宁静无以致远
 */
@EqualsAndHashCode(callSuper = true)
@Data
@Accessors(chain = true)
public class UserAccount extends Model<UserAccount> {

    @TableId
    private Long userId;

    private Long fee;

    private Integer integration;

    private LocalDateTime createTime;

    private LocalDateTime updateTime;

}
