package me.xsomnus.puck.samples.account.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import me.xsomnus.puck.samples.account.entity.UserIntegrationTcc;
import me.xsomnus.puck.samples.account.mapper.UserIntegrationTccMapper;
import me.xsomnus.puck.samples.account.service.IUserIntegrationTccService;
import org.springframework.stereotype.Service;

/**
 * @author @叶小夏
 * @since 2019/8/5 0005 15:03
 * - 才需学也,学需静也/非淡泊无以明志，非宁静无以致远
 */
@Service
public class UserIntegrationTccServiceImpl extends ServiceImpl<UserIntegrationTccMapper, UserIntegrationTcc> implements IUserIntegrationTccService {
}
