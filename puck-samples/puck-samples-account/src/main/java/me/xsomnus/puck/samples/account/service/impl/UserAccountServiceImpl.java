package me.xsomnus.puck.samples.account.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import me.xsomnus.puck.samples.account.entity.UserAccount;
import me.xsomnus.puck.samples.account.mapper.UserAccountMapper;
import me.xsomnus.puck.samples.account.service.IUserAccountService;
import me.xsomnus.puck.samples.common.pojo.request.UserIntegrationResourceReservationRequest;
import me.xsomnus.puck.samples.common.pojo.response.UserIntegrationResourceReservationResponse;
import org.springframework.stereotype.Service;

/**
 * @author @叶小夏
 * @since 2019/8/1 0001 16:04
 * - 才需学也,学需静也/非淡泊无以明志，非宁静无以致远
 */
@Service
public class UserAccountServiceImpl extends ServiceImpl<UserAccountMapper, UserAccount> implements IUserAccountService {
    @Override
    public UserIntegrationResourceReservationResponse reserveIntegration(UserIntegrationResourceReservationRequest request) {
        return null;
    }

    @Override
    public void confirmIntegration(UserIntegrationResourceReservationRequest request) {

    }

    @Override
    public void cancelIntegration(UserIntegrationResourceReservationRequest request) {

    }
}
