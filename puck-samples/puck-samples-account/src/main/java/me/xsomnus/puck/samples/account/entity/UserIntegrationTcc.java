package me.xsomnus.puck.samples.account.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * @author @叶小夏
 * @since 2019/8/5 0005 15:00
 * - 才需学也,学需静也/非淡泊无以明志，非宁静无以致远
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class UserIntegrationTcc extends Model<UserIntegrationTcc> {

    @TableId(type = IdType.ID_WORKER)
    private Long id;

    private String xid;

    private Long userId;

    private Integer integration;
}
