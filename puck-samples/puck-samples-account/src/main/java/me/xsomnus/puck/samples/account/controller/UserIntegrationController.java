package me.xsomnus.puck.samples.account.controller;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import me.xsomnus.puck.samples.account.entity.UserIntegrationTcc;
import me.xsomnus.puck.samples.account.service.IUserAccountService;
import me.xsomnus.puck.samples.account.service.IUserIntegrationTccService;
import me.xsomnus.puck.samples.common.pojo.request.UserIntegrationResourceReservationRequest;
import me.xsomnus.puck.samples.common.pojo.response.UserIntegrationResourceReservationResponse;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author @叶小夏
 * @since 2019/8/5 0005 14:58
 * - 才需学也,学需静也/非淡泊无以明志，非宁静无以致远
 */
@Slf4j
@AllArgsConstructor
@RestController
@RequestMapping("/user-integration")
public class UserIntegrationController {


    private final IUserAccountService iUserAccountService;

    @PutMapping("/reservation")
    public UserIntegrationResourceReservationResponse iTry(@RequestBody UserIntegrationResourceReservationRequest request) {
        return iUserAccountService.reserveIntegration(request);
    }

    @PutMapping("/confirm")
    public void confirm(@RequestBody UserIntegrationResourceReservationRequest request) {
        iUserAccountService.confirmIntegration(request);
    }


    @PutMapping("/cancel")
    public void cancel(@RequestBody UserIntegrationResourceReservationRequest request) {
        iUserAccountService.cancelIntegration(request);
    }

}
