package me.xsomnus.puck.samples.account.service;

import com.baomidou.mybatisplus.extension.service.IService;
import me.xsomnus.puck.samples.account.entity.UserAccount;
import me.xsomnus.puck.samples.common.pojo.request.UserIntegrationResourceReservationRequest;
import me.xsomnus.puck.samples.common.pojo.response.UserIntegrationResourceReservationResponse;

/**
 * @author @叶小夏
 * @since 2019/8/1 0001 16:03
 * - 才需学也,学需静也/非淡泊无以明志，非宁静无以致远
 */
public interface IUserAccountService extends IService<UserAccount> {

    UserIntegrationResourceReservationResponse reserveIntegration(UserIntegrationResourceReservationRequest request);

    void confirmIntegration(UserIntegrationResourceReservationRequest request);

    void cancelIntegration(UserIntegrationResourceReservationRequest request);

}
