package me.xsomnus.puck.samples.account.config;

import com.baomidou.mybatisplus.core.incrementer.IKeyGenerator;
import com.baomidou.mybatisplus.extension.incrementer.PostgreKeyGenerator;
import com.baomidou.mybatisplus.extension.plugins.PaginationInterceptor;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @author @叶小夏
 * @since 2019/8/1 0001 16:05
 * - 才需学也,学需静也/非淡泊无以明志，非宁静无以致远
 */
@Configuration
@MapperScan("me.xsomnus.puck.samples.account.mapper")
public class MybatisPlusConfigure {

    @Bean
    public PaginationInterceptor paginationInterceptor() {
        return new PaginationInterceptor();
    }

    @Bean
    public IKeyGenerator keyGenerator() {
        return new PostgreKeyGenerator();
    }

}
