package me.xsomnus.puck.samples.common.pojo.request;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * @author @叶小夏
 * @since 2019/8/5 0005 15:05
 * - 才需学也,学需静也/非淡泊无以明志，非宁静无以致远
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class UserIntegrationResourceReservationRequest extends ResourceReservationRequest {
    private Integer integration;
}
