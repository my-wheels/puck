package me.xsomnus.puck.samples.common.pojo.request;

import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.List;

/**
 * @author @叶小夏
 * @since 2019/8/2 0002 16:12
 * - 才需学也,学需静也/非淡泊无以明志，非宁静无以致远
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class ProductResourceReservationRequest extends ResourceReservationRequest {


    private List<Product3RItem> items;

    @Data
    public static class Product3RItem {
        private Long productId;
        private Integer quantity;
    }


}
