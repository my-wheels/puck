package me.xsomnus.puck.samples.common.pojo.request;

import lombok.Data;

/**
 * @author @叶小夏
 * @since 2019/8/2 0002 16:12
 * - 才需学也,学需静也/非淡泊无以明志，非宁静无以致远
 */
@Data
public abstract class ResourceReservationRequest {
    private String xid;
}
