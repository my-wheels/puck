package me.xsomnus.puck.samples.common.constants;

/**
 * @author @叶小夏
 * @since 2019/8/1 0001 17:14
 * - 才需学也,学需静也/非淡泊无以明志，非宁静无以致远
 */
public class Service {

    public static final String AccountService = "http://127.0.0.1:11101";

    public static final String MarketService = "http://127.0.0.1:11102";

    public static final String OrdertService = "http://127.0.0.1:11103";

    public static final String ProductService = "http://127.0.0.1:11104";

    public static final String TMService = "http://127.0.0.1:2501";

}
