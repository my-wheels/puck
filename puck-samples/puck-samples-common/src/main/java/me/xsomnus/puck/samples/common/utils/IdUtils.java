package me.xsomnus.puck.samples.common.utils;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.concurrent.atomic.LongAdder;

/**
 * @author @叶小夏
 * @since 2019/8/1 0001 17:22
 * - 才需学也,学需静也/非淡泊无以明志，非宁静无以致远
 */
public class IdUtils {

    private final static LongAdder seq = new LongAdder();

    static {
        seq.add(10000);
    }


    public static String nextOrderId() {
        seq.increment();
        return LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyyMMddHHmmss")) + seq.longValue();
    }


    public static Long nextId() {
        return new IdWorker(0, 0).nextId();
    }

}
