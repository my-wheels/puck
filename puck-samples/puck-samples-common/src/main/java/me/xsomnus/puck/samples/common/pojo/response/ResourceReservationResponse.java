package me.xsomnus.puck.samples.common.pojo.response;

import lombok.Data;

/**
 * @author @叶小夏
 * @since 2019/8/2 0002 16:06
 * - 才需学也,学需静也/非淡泊无以明志，非宁静无以致远
 */
@Data
public abstract class ResourceReservationResponse {

    public boolean ok;

    public String failReason;

}
