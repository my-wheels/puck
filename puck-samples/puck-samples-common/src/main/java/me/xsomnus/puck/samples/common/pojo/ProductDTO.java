package me.xsomnus.puck.samples.common.pojo;

import lombok.Data;
import lombok.experimental.Accessors;

/**
 * @author @叶小夏
 * @since 2019/8/2 0002 16:04
 * - 才需学也,学需静也/非淡泊无以明志，非宁静无以致远
 */
@Data
@Accessors(chain = true)
public class ProductDTO {

    private Long productId;

    private Integer unitPrice;

}
