package me.xsomnus.puck.samples.common.pojo.response;

import lombok.Data;
import lombok.EqualsAndHashCode;
import me.xsomnus.puck.samples.common.pojo.ProductDTO;

import java.util.Map;

/**
 * @author @叶小夏
 * @since 2019/8/2 0002 16:05
 * - 才需学也,学需静也/非淡泊无以明志，非宁静无以致远
 */
@EqualsAndHashCode(callSuper = true)
@Data
public class ProductResourceReservationResponse extends ResourceReservationResponse {

    Map<Long, ProductDTO> productMap;



}
