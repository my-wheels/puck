package me.xsomnus.puck.samples.common.pojo.response;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * @author @叶小夏
 * @since 2019/8/5 0005 10:10
 * - 才需学也,学需静也/非淡泊无以明志，非宁静无以致远
 */
@EqualsAndHashCode(callSuper = true)
@Data
public class UserCouponResourceReservationResponse extends ResourceReservationResponse {

    private Long faceValue;

}
