package me.xsomnus.puck.samples.order.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import me.xsomnus.puck.samples.order.pojo.enums.ShopOrderStatus;

import java.time.LocalDateTime;

/**
 * @author @叶小夏
 * @since 2019/8/1 0001 16:23
 * - 才需学也,学需静也/非淡泊无以明志，非宁静无以致远
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
public class ShopOrder extends Model<ShopOrder> {
    @TableId
    private String orderId;

    private Long userId;

    private Long payableFee;

    private Long realPayFee;

    private ShopOrderStatus status;

    private Integer version;

    private LocalDateTime createTime;

    private LocalDateTime updateTime;
}
