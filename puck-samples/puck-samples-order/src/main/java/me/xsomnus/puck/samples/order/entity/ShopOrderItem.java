package me.xsomnus.puck.samples.order.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * @author @叶小夏
 * @since 2019/8/1 0001 16:25
 * - 才需学也,学需静也/非淡泊无以明志，非宁静无以致远
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
public class ShopOrderItem extends Model<ShopOrderItem> {

    @TableId(type = IdType.ID_WORKER)
    private Long id;
    private String orderId;
    private Long productId;
    private Long unitPrice;
    private Integer quantity;
}
