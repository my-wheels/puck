package me.xsomnus.puck.samples.order.controller;

import lombok.AllArgsConstructor;
import me.xsomnus.puck.samples.order.entity.ShopOrder;
import me.xsomnus.puck.samples.order.pojo.request.PlaceOrderRequest;
import me.xsomnus.puck.samples.order.service.IShopOrderService;
import org.springframework.web.bind.annotation.*;

/**
 * @author @叶小夏
 * @since 2019/8/1 0001 16:40
 * - 才需学也,学需静也/非淡泊无以明志，非宁静无以致远
 */
@AllArgsConstructor
@RestController
@RequestMapping("/shop-order")
public class ShopOrderController {

    private final IShopOrderService iShopOrderService;


    /**
     * 下单(try)
     */
    @PostMapping
    public String placeOrder(@RequestBody PlaceOrderRequest placeOrderRequest) {
        return iShopOrderService.placeOrder(placeOrderRequest);
    }

    /**
     * 模拟支付成功回调(confirm)
     */
    @PutMapping("/pay-success")
    public boolean paySuccess() {
        return false;
    }


    /**
     * 模拟支付超时(cancel)
     */
    @PutMapping("/pay-timeout")
    public boolean timeout() {
        return false;
    }


    @GetMapping("/{orderId}")
    public ShopOrder getById(@PathVariable String orderId) {
        return iShopOrderService.getById(orderId);
    }

}
