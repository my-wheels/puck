package me.xsomnus.puck.samples.order.pojo.enums;

import com.baomidou.mybatisplus.core.enums.IEnum;

/**
 * @author @叶小夏
 * @since 2019/8/1 0001 16:27
 * - 才需学也,学需静也/非淡泊无以明志，非宁静无以致远
 */
public enum ShopOrderStatus implements IEnum<Short> {

    WAITING_PAY((short) 100),
    CANCELLED((short) 101),
    PAYED((short) 200);

    private final short code;

    ShopOrderStatus(short code) {
        this.code = code;
    }

    @Override
    public Short getValue() {
        return code;
    }
}
