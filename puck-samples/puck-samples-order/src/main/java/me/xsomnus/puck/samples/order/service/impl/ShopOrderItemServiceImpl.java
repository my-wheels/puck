package me.xsomnus.puck.samples.order.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import me.xsomnus.puck.samples.order.entity.ShopOrderItem;
import me.xsomnus.puck.samples.order.mapper.ShopOrderItemMapper;
import me.xsomnus.puck.samples.order.service.IShopOrderItemService;
import org.springframework.stereotype.Service;

/**
 * @author @叶小夏
 * @since 2019/8/1 0001 16:26
 * - 才需学也,学需静也/非淡泊无以明志，非宁静无以致远
 */
@Service
public class ShopOrderItemServiceImpl extends ServiceImpl<ShopOrderItemMapper, ShopOrderItem> implements IShopOrderItemService {
}
