package me.xsomnus.puck.samples.order.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import me.xsomnus.puck.samples.order.entity.ShopOrder;

/**
 * @author @叶小夏
 * @since 2019/8/1 0001 16:30
 * - 才需学也,学需静也/非淡泊无以明志，非宁静无以致远
 */
public interface ShopOrderMapper extends BaseMapper<ShopOrder> {
}
