package me.xsomnus.puck.samples.order.pojo.request;

import lombok.Data;

import java.util.List;

/**
 * @author @叶小夏
 * @since 2019/8/1 0001 16:41
 * - 才需学也,学需静也/非淡泊无以明志，非宁静无以致远
 */
@Data
public class PlaceOrderRequest {

    private Long couponId;

    private Long userId;

    private List<OrderItemRequest> items;
}
