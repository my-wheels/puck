package me.xsomnus.puck.samples.order.service;

import com.baomidou.mybatisplus.extension.service.IService;
import me.xsomnus.puck.samples.order.entity.ShopOrderItem;

/**
 * @author @叶小夏
 * @since 2019/8/1 0001 16:26
 * - 才需学也,学需静也/非淡泊无以明志，非宁静无以致远
 */
public interface IShopOrderItemService extends IService<ShopOrderItem> {
}
