package me.xsomnus.puck.samples.order.service.impl;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import me.xsomnus.puck.samples.common.utils.IdUtils;
import me.xsomnus.puck.samples.order.entity.ShopOrder;
import me.xsomnus.puck.samples.order.entity.ShopOrderItem;
import me.xsomnus.puck.samples.order.mapper.ShopOrderMapper;
import me.xsomnus.puck.samples.order.pojo.enums.ShopOrderStatus;
import me.xsomnus.puck.samples.order.pojo.request.PlaceOrderRequest;
import me.xsomnus.puck.samples.order.service.IShopOrderItemService;
import me.xsomnus.puck.samples.order.service.IShopOrderService;
import okhttp3.*;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.io.IOException;
import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

/**
 * @author @叶小夏
 * @since 2019/8/1 0001 16:30
 * - 才需学也,学需静也/非淡泊无以明志，非宁静无以致远
 */
@Slf4j
@Service
@AllArgsConstructor
public class ShopOrderServiceImpl extends ServiceImpl<ShopOrderMapper, ShopOrder> implements IShopOrderService {

    private static OkHttpClient okHttpClient = new OkHttpClient.Builder()
            .connectTimeout(5, TimeUnit.SECONDS)
            .build();
    private final IShopOrderItemService iShopOrderItemService;

    @Override
    @Transactional(rollbackFor = Exception.class)
    public String placeOrder(PlaceOrderRequest placeOrderRequest) {
        String orderId = IdUtils.nextOrderId();
        ShopOrder shopOrder = new ShopOrder()
                .setOrderId(orderId)
                .setStatus(ShopOrderStatus.WAITING_PAY)
                .setUserId(placeOrderRequest.getUserId());
        List<ShopOrderItem> shopOrderItems = placeOrderRequest.getItems().stream()
                .map(item -> new ShopOrderItem()
                        .setOrderId(orderId)
                        .setProductId(item.getProductId())
                        .setQuantity(item.getQuantity())
                ).collect(Collectors.toList());
        this.save(shopOrder);
        iShopOrderItemService.saveBatch(shopOrderItems);


        //todo 这一部分可以通过定义注解来实现
        JSONArray jsonArray = new JSONArray();

        JSONObject productStockJsonObj = new JSONObject();
        JSONObject marketJsonObj = new JSONObject();

        productStockJsonObj.put("uri", me.xsomnus.puck.samples.common.constants.Service.ProductService + "/product");
        productStockJsonObj.put("name", "商品库存服务");
        productStockJsonObj.put("leader", false);

        marketJsonObj.put("uri", me.xsomnus.puck.samples.common.constants.Service.MarketService + "/market/user-coupon");
        marketJsonObj.put("name", "优惠券核销");
        marketJsonObj.put("leader", false);
        jsonArray.add(productStockJsonObj);
        jsonArray.add(marketJsonObj);


        JSONObject postObj = new JSONObject();
        postObj.put("name", "订单[" + orderId + "]下单");
        postObj.put("groupId", "placeOrder");
        postObj.put("participants", jsonArray);

        String jsonStr = postObj.toJSONString();

        final Call call = okHttpClient.newCall(new Request.Builder()
                .url(me.xsomnus.puck.samples.common.constants.Service.TMService + "/api/v1/tm/initiate")
                .post(RequestBody.create(MediaType.parse("application/json"), jsonStr))
                .build());
        try {
            final Response response = call.execute();
            assert response.body() != null;
            final String res = response.body().string();
            log.debug("请求体： {}, 发起事务的响应结果---{}", jsonStr, res);
            JSONObject resJson = JSONObject.parseObject(res);
            String xid = resJson.getString("xid");
            // Mark --- 商品资源预留
            JSONObject productRequestObj = new JSONObject();
            productRequestObj.put("xid", xid);
            JSONArray itemArr = new JSONArray();

            placeOrderRequest.getItems().forEach(item -> {
                JSONObject jsonObject = new JSONObject();
                jsonObject.put("product_id", item.getProductId());
                jsonObject.put("quantity", item.getQuantity());
                itemArr.add(jsonObject);
            });
            productRequestObj.put("items", itemArr);

            final Call productCall = okHttpClient.newCall(new Request.Builder()
                    .url(me.xsomnus.puck.samples.common.constants.Service.ProductService + "/product/reservation")
                    .put(RequestBody.create(MediaType.parse("application/json"), productRequestObj.toJSONString()))
                    .build());

            final Response productResponse = productCall.execute();
            assert productResponse.body() != null;
            boolean isOk = checkResourceReservationResponse(productResponse.body().string());
            boolean isOk2 = true;
            if (placeOrderRequest.getCouponId() != null) {
                // Mark --- 预留优惠券资源
                JSONObject marketRequestObj = new JSONObject();
                marketRequestObj.put("xid", xid);
                marketRequestObj.put("user_id", placeOrderRequest.getUserId());
                marketRequestObj.put("coupon_id", placeOrderRequest.getCouponId());
                final Call marketCall = okHttpClient.newCall(new Request.Builder()
                        .url(me.xsomnus.puck.samples.common.constants.Service.MarketService + "/market/user-coupon/reservation")
                        .put(RequestBody.create(MediaType.parse("application/json"), marketRequestObj.toJSONString()))
                        .build());
                final Response marketResponse = marketCall.execute();
                isOk2 = checkResourceReservationResponse(marketResponse.body().string());
            }
            if (isOk && isOk2) {
                log.debug("资源预留成功");
            }

        } catch (IOException e) {
            e.printStackTrace();
        }
        return orderId;
    }


    private boolean checkResourceReservationResponse(String ret) {
        JSONObject jsonObject = JSONObject.parseObject(ret);
        final Boolean ok = jsonObject.getBoolean("ok");
        log.debug("ret === " + ret);
        if (!ok) {
            log.debug("3r is not ok, reason is " + jsonObject.getString("fail_reason"));
        }
        return ok;
    }
}
