package me.xsomnus.puck.samples.market.controller;

import lombok.AllArgsConstructor;
import me.xsomnus.puck.samples.common.pojo.request.UserCouponResourceReservationRequest;
import me.xsomnus.puck.samples.common.pojo.response.UserCouponResourceReservationResponse;
import me.xsomnus.puck.samples.market.entity.UserCoupon;
import me.xsomnus.puck.samples.market.service.IUserCouponService;
import org.springframework.web.bind.annotation.*;

/**
 * @author @叶小夏
 * @since 2019/8/1 0001 17:51
 * - 才需学也,学需静也/非淡泊无以明志，非宁静无以致远
 */
@RestController
@AllArgsConstructor
@RequestMapping("/market/user-coupon")
public class UserCouponController {

    private final IUserCouponService iUserCouponService;


    @GetMapping("/{id}")
    public UserCoupon getById(@PathVariable Long id) {
        return iUserCouponService.getById(id);
    }


    @PutMapping("/reservation")
    public UserCouponResourceReservationResponse iTry(@RequestBody UserCouponResourceReservationRequest request) {
        return iUserCouponService.reserve(request);
    }

    @PutMapping("/confirm")
    public void confirm(@RequestBody UserCouponResourceReservationRequest request) {
       iUserCouponService.confirm(request);
    }


    @PutMapping("/cancel")
    public void cancel(@RequestBody UserCouponResourceReservationRequest request) {
        iUserCouponService.cancel(request);
    }

}
