package me.xsomnus.puck.samples.market.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import me.xsomnus.puck.samples.market.entity.UserCoupon;

/**
 * @author @叶小夏
 * @since 2019/8/1 0001 16:17
 * - 才需学也,学需静也/非淡泊无以明志，非宁静无以致远
 */
public interface UserCouponMapper extends BaseMapper<UserCoupon> {
}
