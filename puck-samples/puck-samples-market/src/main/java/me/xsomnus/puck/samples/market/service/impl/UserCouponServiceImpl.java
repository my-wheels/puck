package me.xsomnus.puck.samples.market.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import lombok.AllArgsConstructor;
import me.xsomnus.puck.samples.common.pojo.request.UserCouponResourceReservationRequest;
import me.xsomnus.puck.samples.common.pojo.response.UserCouponResourceReservationResponse;
import me.xsomnus.puck.samples.market.entity.UserCoupon;
import me.xsomnus.puck.samples.market.entity.UserCouponTcc;
import me.xsomnus.puck.samples.market.mapper.UserCouponMapper;
import me.xsomnus.puck.samples.market.pojo.enums.TccStatus;
import me.xsomnus.puck.samples.market.pojo.enums.UserCouponStatus;
import me.xsomnus.puck.samples.market.service.IUserCouponService;
import me.xsomnus.puck.samples.market.service.IUserCouponTccService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

/**
 * @author @叶小夏
 * @since 2019/8/1 0001 16:18
 * - 才需学也,学需静也/非淡泊无以明志，非宁静无以致远
 */
@Service
@AllArgsConstructor
public class UserCouponServiceImpl extends ServiceImpl<UserCouponMapper, UserCoupon> implements IUserCouponService {


    private final IUserCouponTccService iUserCouponTccService;

    private UserCoupon getByUserIdAndCouponId(Long userId, String couponId) {
        return this.getOne(new QueryWrapper<UserCoupon>().eq("user_id", userId).eq("coupon_id", couponId));
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public UserCouponResourceReservationResponse reserve(UserCouponResourceReservationRequest request) {
        final UserCouponResourceReservationResponse response = new UserCouponResourceReservationResponse();
        final UserCoupon userCoupon = this.getByUserIdAndCouponId(request.getUserId(), request.getCouponId());

        if (userCoupon == null) {
            response.setOk(false);
            response.setFailReason("优惠券不存在");
            return response;
        }

        if (!userCoupon.getStatus().equals(UserCouponStatus.PENDING)) {
            response.setOk(false);
            response.setFailReason("优惠券已经被使用");
            return response;
        }

        UserCoupon tmp = new UserCoupon().setId(userCoupon.getId()).setStatus(UserCouponStatus.LOCKED);
        boolean addTcc = iUserCouponTccService.save(new UserCouponTcc().setUserCouponId(userCoupon.getId()).setXid(request.getXid()).setStatus(TccStatus.TRY));
        boolean updateCoupon = this.updateById(tmp);
        response.setOk(addTcc && updateCoupon);
        response.setFaceValue(tmp.getFaceFee());
        return response;
    }

    @Override
    public void confirm(UserCouponResourceReservationRequest request) {
        final List<UserCouponTcc> userCouponTccs = iUserCouponTccService.listByXid(request.getXid());

        List<UserCouponTcc> userCouponTccList = new ArrayList<>(userCouponTccs.size());
        List<UserCoupon> userCouponList = new ArrayList<>(userCouponTccs.size());
        userCouponTccs.forEach(tcc -> {
            UserCouponTcc userCouponTcc = new UserCouponTcc().setId(tcc.getId()).setStatus(TccStatus.CONFIRM);
            userCouponList.add(new UserCoupon().setId(tcc.getUserCouponId()).setStatus(UserCouponStatus.USED));
            userCouponTccList.add(userCouponTcc);
        });
        iUserCouponTccService.updateBatchById(userCouponTccList);
        this.updateBatchById(userCouponList);
    }

    @Override
    public void cancel(UserCouponResourceReservationRequest request) {
        final List<UserCouponTcc> userCouponTccs = iUserCouponTccService.listByXid(request.getXid());
        List<UserCouponTcc> userCouponTccList = new ArrayList<>(userCouponTccs.size());
        List<UserCoupon> userCouponList = new ArrayList<>(userCouponTccs.size());
        userCouponTccs.forEach(tcc -> {
            UserCouponTcc userCouponTcc = new UserCouponTcc().setId(tcc.getId()).setStatus(TccStatus.CANCEL);
            userCouponList.add(new UserCoupon().setId(tcc.getUserCouponId()).setStatus(UserCouponStatus.PENDING));
            userCouponTccList.add(userCouponTcc);
        });
        iUserCouponTccService.updateBatchById(userCouponTccList);
        this.updateBatchById(userCouponList);
    }
}
