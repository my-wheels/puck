package me.xsomnus.puck.samples.market.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import me.xsomnus.puck.samples.market.pojo.enums.UserCouponStatus;

import java.time.LocalDateTime;

/**
 * @author @叶小夏
 * @since 2019/8/1 0001 16:12
 * - 才需学也,学需静也/非淡泊无以明志，非宁静无以致远
 */
@EqualsAndHashCode(callSuper = true)
@Data
@Accessors(chain = true)
public class UserCoupon extends Model<UserCoupon> {

    @TableId
    private Long id;

    private Long userId;

    private Long couponId;

    private Long faceFee;

    private UserCouponStatus status;

    private LocalDateTime createTime;

    private LocalDateTime updateTime;
}
