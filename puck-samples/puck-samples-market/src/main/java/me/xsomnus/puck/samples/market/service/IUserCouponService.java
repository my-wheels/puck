package me.xsomnus.puck.samples.market.service;

import com.baomidou.mybatisplus.extension.service.IService;
import me.xsomnus.puck.samples.common.pojo.request.UserCouponResourceReservationRequest;
import me.xsomnus.puck.samples.common.pojo.response.UserCouponResourceReservationResponse;
import me.xsomnus.puck.samples.market.entity.UserCoupon;

/**
 * @author @叶小夏
 * @since 2019/8/1 0001 16:18
 * - 才需学也,学需静也/非淡泊无以明志，非宁静无以致远
 */
public interface IUserCouponService extends IService<UserCoupon> {

    UserCouponResourceReservationResponse reserve(UserCouponResourceReservationRequest request);

    void confirm(UserCouponResourceReservationRequest request);

    void cancel(UserCouponResourceReservationRequest request);

}
