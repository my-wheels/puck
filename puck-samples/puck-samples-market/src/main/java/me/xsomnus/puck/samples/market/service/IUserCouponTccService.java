package me.xsomnus.puck.samples.market.service;

import com.baomidou.mybatisplus.extension.service.IService;
import me.xsomnus.puck.samples.market.entity.UserCouponTcc;

import java.util.List;

/**
 * @author @叶小夏
 * @since 2019/8/5 0005 10:43
 * - 才需学也,学需静也/非淡泊无以明志，非宁静无以致远
 */
public interface IUserCouponTccService extends IService<UserCouponTcc> {
    List<UserCouponTcc> listByXid(String xid);
}
