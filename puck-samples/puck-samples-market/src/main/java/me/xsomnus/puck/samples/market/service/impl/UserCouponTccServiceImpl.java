package me.xsomnus.puck.samples.market.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import me.xsomnus.puck.samples.market.entity.UserCouponTcc;
import me.xsomnus.puck.samples.market.mapper.UserCouponTccMapper;
import me.xsomnus.puck.samples.market.service.IUserCouponTccService;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author @叶小夏
 * @since 2019/8/5 0005 10:44
 * - 才需学也,学需静也/非淡泊无以明志，非宁静无以致远
 */
@Service
public class UserCouponTccServiceImpl extends ServiceImpl<UserCouponTccMapper, UserCouponTcc> implements IUserCouponTccService {
    @Override
    public List<UserCouponTcc> listByXid(String xid) {
        return this.list(new QueryWrapper<UserCouponTcc>().eq("xid", xid));
    }
}
