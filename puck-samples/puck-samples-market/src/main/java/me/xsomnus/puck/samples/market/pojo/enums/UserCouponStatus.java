package me.xsomnus.puck.samples.market.pojo.enums;

import com.baomidou.mybatisplus.core.enums.IEnum;

/**
 * @author @叶小夏
 * @since 2019/8/1 0001 16:14
 * - 才需学也,学需静也/非淡泊无以明志，非宁静无以致远
 */
public enum UserCouponStatus implements IEnum<Short> {
    // Mark --- 待使用
    PENDING((short) 100),
    // Mark --- 锁定
    LOCKED((short) 101),
    // Mark --- 已使用
    USED((short) 102);

    private final short code;

    UserCouponStatus(short code) {
        this.code = code;
    }

    @Override
    public Short getValue() {
        return code;
    }
}
