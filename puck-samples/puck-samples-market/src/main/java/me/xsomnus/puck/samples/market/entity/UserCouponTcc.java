package me.xsomnus.puck.samples.market.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import me.xsomnus.puck.samples.market.pojo.enums.TccStatus;

/**
 * @author @叶小夏
 * @since 2019/8/5 0005 10:40
 * - 才需学也,学需静也/非淡泊无以明志，非宁静无以致远
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
public class UserCouponTcc extends Model<UserCouponTcc> {

    @TableId(type = IdType.ID_WORKER)
    private Long id;
    private Long userCouponId;
    private String xid;
    private TccStatus status;
}
