package me.xsomnus.puck.samples.market.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import me.xsomnus.puck.samples.market.entity.UserCouponTcc;

/**
 * @author @叶小夏
 * @since 2019/8/5 0005 10:43
 * - 才需学也,学需静也/非淡泊无以明志，非宁静无以致远
 */
public interface UserCouponTccMapper extends BaseMapper<UserCouponTcc> {
}
