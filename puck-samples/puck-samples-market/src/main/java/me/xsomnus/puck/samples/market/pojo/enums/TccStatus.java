package me.xsomnus.puck.samples.market.pojo.enums;

import com.baomidou.mybatisplus.core.enums.IEnum;

/**
 * @author @叶小夏
 * @since 2019/8/5 0005 10:41
 * - 才需学也,学需静也/非淡泊无以明志，非宁静无以致远
 */
public enum TccStatus implements IEnum<Short> {
    TRY((short) 1),
    CONFIRM((short) 2),
    CANCEL((short) 3);

    private final short code;

    TccStatus(short code) {
        this.code = code;
    }

    @Override
    public Short getValue() {
        return code;
    }
}