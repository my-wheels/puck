/*
 Navicat Premium Data Transfer

 Source Server         : test-mysql-maynrent
 Source Server Type    : MySQL
 Source Server Version : 80016
 Source Host           : 192.168.0.227:3306
 Source Schema         : puck-account

 Target Server Type    : MySQL
 Target Server Version : 80016
 File Encoding         : 65001

 Date: 01/08/2019 18:15:12
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for user_account
-- ----------------------------
DROP TABLE IF EXISTS `user_account`;
CREATE TABLE `user_account`  (
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `fee` bigint(20) UNSIGNED DEFAULT 0,
  `create_time` datetime(0) DEFAULT CURRENT_TIMESTAMP,
  `update_time` datetime(0) DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP(0),
  PRIMARY KEY (`user_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of user_account
-- ----------------------------
INSERT INTO `user_account` VALUES (1, 10000, '2019-08-01 15:30:23', '2019-08-01 15:30:23');
INSERT INTO `user_account` VALUES (2, 100, '2019-08-01 15:30:27', '2019-08-01 15:30:27');

SET FOREIGN_KEY_CHECKS = 1;
