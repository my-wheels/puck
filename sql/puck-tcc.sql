/*
 Navicat Premium Data Transfer

 Source Server         : 阿里云服务
 Source Server Type    : MySQL
 Source Server Version : 80015
 Source Host           : 39.108.117.147:4306
 Source Schema         : mous-tcc

 Target Server Type    : MySQL
 Target Server Version : 80015
 File Encoding         : 65001

 Date: 02/08/2019 09:52:41
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for tcc_participant
-- ----------------------------
DROP TABLE IF EXISTS `tcc_participant`;
CREATE TABLE `tcc_participant`  (
  `id` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '参与方id',
  `xid` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '参与的事务编号',
  `role` smallint(4) unsigned default 0 comment '0-leader,1-follower',
  `rpc_type` smallint(4) UNSIGNED DEFAULT 100 COMMENT 'RPC类型100-http,102-dubbo,103-grpc,104-thrift,200-自定义',
  `name` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT '' COMMENT '服务名称',
  `url` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT '' COMMENT '资源url地址',
  `request_body` varchar(512) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT '' COMMENT '请求对象',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '事务参与者' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for tcc_transaction
-- ----------------------------
DROP TABLE IF EXISTS `tcc_transaction`;
CREATE TABLE `tcc_transaction`  (
  `xid` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '事务编号',
  `name` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT 'TCC事务' COMMENT '事务名称',
  `group_id` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '事务分组',
  `max_retry_times` smallint(4) unsigned DEFAULT 5 COMMENT '事务最大重试次数',
  `retried_times` smallint(4) DEFAULT 0 COMMENT '已经重试次数',
  `retry_interval` smallint(4) DEFAULT 15 COMMENT '重试间隔',
  `next_retry_time` datetime(0) DEFAULT '1972-01-01 00:00:00' COMMENT '下次重试的时间',
  `status` smallint(4) UNSIGNED DEFAULT 100 COMMENT '事务状态(try,confirm,cancel)',
  `enabled` tinyint(1) DEFAULT 1 COMMENT '事务是否可用',
  `deleted` tinyint(1) DEFAULT 0 COMMENT '事务是否删除',
  `version` int(11) UNSIGNED DEFAULT 0 COMMENT '版本号',
  `create_time` datetime(0) DEFAULT CURRENT_TIMESTAMP COMMENT '事务创建时间',
  `update_time` datetime(0) DEFAULT CURRENT_TIMESTAMP COMMENT '最后一次更新时间',
  PRIMARY KEY (`xid`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '事务列表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for tcc_transaction_log
-- ----------------------------
DROP TABLE IF EXISTS `tcc_transaction_log`;
CREATE TABLE `tcc_transaction_log`  (
  `id` bigint(20) NOT NULL COMMENT '记录编号',
  `xid` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '事务编号',
  `participant_id` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '参与方编号',
  `execution_status` smallint(4) NOT NULL COMMENT '执行状态（try,confirm,cancel)',
  `execution_result` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT '' COMMENT '执行的结果（超时，OK之类的）',
  `execution_times_seq` smallint(4) DEFAULT 0 COMMENT '重试的第几次,0代表首次执行',
  `fail_cause` varchar(256) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT '' COMMENT '失败原因',
  `success` tinyint(1) DEFAULT 0 COMMENT '执行是否成功',
  `create_time` datetime(0) DEFAULT CURRENT_TIMESTAMP COMMENT '添加时间',
  `update_time` datetime(0) DEFAULT CURRENT_TIMESTAMP on update current_timestamp COMMENT '更新时间',
  `start_time` datetime(0) DEFAULT '1970-01-01 00:00:00' COMMENT '开始时间',
  `end_time` datetime(0) DEFAULT '1970-01-01 00:00:00' COMMENT '结束时间',
  `final_result` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT '' COMMENT '此次事务执行最后结果',
  PRIMARY KEY (`id`, `xid`, `participant_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '事务执行记录' ROW_FORMAT = Dynamic;

SET FOREIGN_KEY_CHECKS = 1;
