/*
 Navicat Premium Data Transfer

 Source Server         : test-mysql-maynrent
 Source Server Type    : MySQL
 Source Server Version : 80016
 Source Host           : 192.168.0.227:3306
 Source Schema         : puck-product

 Target Server Type    : MySQL
 Target Server Version : 80016
 File Encoding         : 65001

 Date: 02/08/2019 18:20:16
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for product
-- ----------------------------
DROP TABLE IF EXISTS `product`;
CREATE TABLE `product`  (
  `id` bigint(20) NOT NULL,
  `name` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '',
  `stock` int(11) UNSIGNED DEFAULT 0,
  `total_reserve_stock` int(11) UNSIGNED DEFAULT 0,
  `unit_price` int(11) UNSIGNED DEFAULT 0,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of product
-- ----------------------------
INSERT INTO `product` VALUES (1, '苹果6s', 100, 4, 1000);
INSERT INTO `product` VALUES (2, '茶杯', 100, 44, 50);
INSERT INTO `product` VALUES (3, '笔记本', 100, 0, 10);
INSERT INTO `product` VALUES (4, '100', 100, 0, 100);

-- ----------------------------
-- Table structure for product_stock_tcc
-- ----------------------------
DROP TABLE IF EXISTS `product_stock_tcc`;
CREATE TABLE `product_stock_tcc`  (
  `id` bigint(20) NOT NULL,
  `xid` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `product_id` bigint(20) DEFAULT NULL,
  `stock` int(11) UNSIGNED DEFAULT 0,
  `status` smallint(4) UNSIGNED DEFAULT 0,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `xid_product_id_unique`(`xid`, `product_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of product_stock_tcc
-- ----------------------------
INSERT INTO `product_stock_tcc` VALUES (1157231693992206337, '10001', 1, 2, 0);
INSERT INTO `product_stock_tcc` VALUES (1157231694000594946, '10001', 2, 22, 0);
INSERT INTO `product_stock_tcc` VALUES (1157231878210232321, '10002', 1, 2, 0);
INSERT INTO `product_stock_tcc` VALUES (1157231878218620929, '10002', 2, 22, 0);

SET FOREIGN_KEY_CHECKS = 1;
