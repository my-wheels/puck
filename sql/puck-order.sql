/*
 Navicat Premium Data Transfer

 Source Server         : test-mysql-maynrent
 Source Server Type    : MySQL
 Source Server Version : 80016
 Source Host           : 192.168.0.227:3306
 Source Schema         : puck-order

 Target Server Type    : MySQL
 Target Server Version : 80016
 File Encoding         : 65001

 Date: 01/08/2019 18:14:57
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for shop_order
-- ----------------------------
DROP TABLE IF EXISTS `shop_order`;
CREATE TABLE `shop_order`  (
  `order_id` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `user_id` bigint(20) NOT NULL,
  `payable_fee` bigint(20) UNSIGNED DEFAULT 0,
  `real_pay_fee` bigint(20) UNSIGNED DEFAULT 0,
  `status` smallint(4) UNSIGNED NOT NULL DEFAULT 100,
  `version` int(11) UNSIGNED DEFAULT 0,
  `create_time` datetime(0) DEFAULT CURRENT_TIMESTAMP,
  `update_time` datetime(0) DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP(0),
  PRIMARY KEY (`order_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of shop_order
-- ----------------------------
INSERT INTO `shop_order` VALUES ('2019080117441710002', 1, 0, 0, 100, 0, '2019-08-01 17:44:21', '2019-08-01 17:44:21');
INSERT INTO `shop_order` VALUES ('2019080118015310001', 1, 0, 0, 100, 0, '2019-08-01 18:01:54', '2019-08-01 18:01:54');

-- ----------------------------
-- Table structure for shop_order_item
-- ----------------------------
DROP TABLE IF EXISTS `shop_order_item`;
CREATE TABLE `shop_order_item`  (
  `id` bigint(20) NOT NULL,
  `order_id` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `product_id` bigint(20) UNSIGNED NOT NULL,
  `unit_price` bigint(20) UNSIGNED NOT NULL DEFAULT 0,
  `quantity` int(11) UNSIGNED NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of shop_order_item
-- ----------------------------
INSERT INTO `shop_order_item` VALUES (308901514649272320, '2019080117441710002', 1, 0, 1);
INSERT INTO `shop_order_item` VALUES (308905931029086208, '2019080118015310001', 1, 0, 1);

SET FOREIGN_KEY_CHECKS = 1;
