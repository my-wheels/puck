/*
 Navicat Premium Data Transfer

 Source Server         : test-mysql-maynrent
 Source Server Type    : MySQL
 Source Server Version : 80016
 Source Host           : 192.168.0.227:3306
 Source Schema         : puck-market

 Target Server Type    : MySQL
 Target Server Version : 80016
 File Encoding         : 65001

 Date: 01/08/2019 18:14:49
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for user_coupon
-- ----------------------------
DROP TABLE IF EXISTS `user_coupon`;
CREATE TABLE `user_coupon`  (
  `id` bigint(20) NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `coupon_id` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `face_fee` bigint(20) UNSIGNED DEFAULT 0,
  `status` smallint(4) NOT NULL,
  `create_time` datetime(0) DEFAULT CURRENT_TIMESTAMP,
  `update_time` datetime(0) DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP(0),
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of user_coupon
-- ----------------------------
INSERT INTO `user_coupon` VALUES (1, 1, '1', 20, 100, '2019-08-01 15:32:25', '2019-08-01 15:32:25');
INSERT INTO `user_coupon` VALUES (2, 1, '2', 500, 100, '2019-08-01 15:32:37', '2019-08-01 15:32:37');

SET FOREIGN_KEY_CHECKS = 1;
